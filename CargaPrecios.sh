
filename=$(basename $0)
nameETL="${filename%.*}"

export DIR_ETL=./$nameETL
export DIR_CONFIG=/recovery/haya/programas_ext/software/binarios/
export CFG_FILE=config.ini
export MAINSH="$nameETL"_run.sh

cd "$DIR_ETL" &> /dev/null
if [ $? -ne 0 ] ; then
   echo "$(basename $0) Error en $filename: directorio inexistente $DIR_ETL"
   exit 1
fi

cat $DIR_CONFIG$CFG_FILE

if [ -f $MAINSH ]; then
    CLASS="$(cat $MAINSH | grep "^java" | cut -f7 -d" ")"
echo CLASS=$CLASS
    CLASS2=`echo $CLASS | sed -e 's/$ROOT_PATH/./g'`
echo CLASS2=$CLASS2
    CLASEINICIO="$(cat $MAINSH | grep "^java" | cut -f8 -d" ")"
echo CLASEINICIO=$CLASEINICIO
    java -Xms512M -Xmx1536M -Dconfig.dir=$DIR_CONFIG -Dconfig.file.mask=$CFG_FILE -Duser.country=ES -Duser.language=es -cp $CLASS2 $CLASEINICIO  --context=Formacion "$@" 
    exit $?
else
    echo "$(basename $0) Error en $filename: no se ha encontrado  $MAINSH"
    exit 1
fi
