package routines;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class cambiaString {

    /**
     * helloExample: not return value, only print "hello" + message.
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */
    public static void helloExample(String message) {
        if (message == null) {
            message = "World"; //$NON-NLS-1$
        }
        System.out.println("Hello " + message + " !"); //$NON-NLS-1$ //$NON-NLS-2$
    }
    /** 
     * cambiaStringF: un string "yyyy-MM-dd" lo cambiamos "yyyyMMdd"
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} String fecha: un string "yyyy-MM-dd" lo cambiamos "yyyyMMdd"
     * 
     * {example} cambiaStringF("2017-03-27") # 20170327 !.
     * si tengo un string "yyyy-MM-dd" lo cambiamos "yyyyMMdd"
     * @param fecha
     * @return
     */
    public static String cambiaStringF(String fecha)
    {
    		System.out.println("fecha:  "+ fecha);
			if (!fecha.isEmpty())
			{
				String[] miaux= fecha.split("-");
				System.out.println("miaux:  "+  miaux[2] +"*"+ miaux[1] +"*"+ miaux[0]);
				fecha= miaux[0] + miaux[1] + miaux[2];
				System.out.println("cambio: "+fecha);
				
			}
			
			return fecha;
		
    }
    /** 
     * cambiaStringF: un string "RES: 101939 | OFR: 2222222" lo cambiamos "101939"
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} String campo: un string "RES: 101939 | OFR: 2222222" lo cambiamos "101939"
     * 
     * {example} cambiaStringF("RES: 101939 | OFR: 2222222") # 101939 !.
     * un string "RES: 101939 | OFR: 2222222" lo cambiamos "101939"
     * @param campo
     * @return
     */
    public static int cambiaNumres(String campo)
    {
    	int resul=0;
    	
    	if (campo!= null ) 
		{
			if(!campo.isEmpty())
			{
				int miaux= campo.indexOf("|");
				String numReserva = campo.substring(0,miaux-1);
				System.out.println("numReserva:  "+numReserva.substring(4));
				campo = numReserva.substring(5);
				resul = Integer.parseInt(campo);
				System.out.println("numReserva:  "+resul);
			}
		}
			
			return resul;
		
    }
    
    /** 
     * cambiaStringF: un string "RES: 101939 | OFR: 2222222" lo cambiamos "2222222"
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} String campo: un string "RES: 101939 | OFR: 2222222" lo cambiamos "2222222"
     * 
     * {example} cambiaStringF("RES: 101939 | OFR: 2222222") # 2222222 !.
     * un string "RES: 101939 | OFR: 2222222" lo cambiamos "2222222"
     * @param campo
     * @return
     */
    public static int cambiaNumOfer(String campo)
    {
    	int resul=0;
    	
    	if (campo!= null ) 
		{
			if(!campo.isEmpty())
			{
				int miaux= campo.indexOf("|");
				String numReserva = campo.substring(miaux+2);
				System.out.println("oferta:  "+numReserva.substring(4));
				campo = numReserva.substring(5);
				resul = Integer.parseInt(campo);
				System.out.println("oferta:  "+resul);
			}
		}
			return resul;
		
    }
    
    /** 
     * cambiaStringToInt: un string "101939" lo cambiamos 101939
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} String campo: un string "101939" lo cambiamos 101939
     * 
     * {example} cambiaStringToInt("2222222") # 2222222 !.
     * un string "2222222" lo cambiamos 2222222
     * @param campo
     * @return
     */
    public static int cambiaStringToInt(String campo)
    {
    	int resul=0;
    	
    	if (campo!= null ) 
		{
			if(!campo.isEmpty())
			{
				resul = Integer.parseInt(campo);
				System.out.println("activo:  "+resul);
			}
		}
			return resul;
		
    }
    
    
    
    /** 
     * cambiaStringAct: un string "101939|22" lo cambiamos "101939"
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} String campo: un string "101939|22" lo cambiamos "101939"
     * 
     * {example} cambiaStringAct("101939|22") # 101939 !.
     * un string "101939|22" lo cambiamos "101939"
     * @param campo
     * @return
     */
    public static int cambiaStringAct(String campo)
    {
    	int resul=0;
    	
    	if (campo!= null ) 
		{
			if(!campo.isEmpty())
			{
				int miaux= campo.indexOf("|");
				String act = campo.substring(0,miaux);
				System.out.println("act:  "+act);
				resul = Integer.parseInt(act);
				System.out.println("act:  "+act);
			}
		}
			
			return resul;
		
    }
    
    /** 
     * cambiaStringTipoPrecio: un string "101939|22" lo cambiamos "22"
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} String campo: un string "101939|22" lo cambiamos "22"
     * 
     * {example} cambiaStringTipoPrecio("101939|22") # 22 !.
     * un string "101939|22" lo cambiamos "22"
     * @param campo
     * @return
     */
    public static String cambiaStringTipoPrecio(String campo)
    {
    	//int resul=0;
    	String tipoPre = "";
    	if (campo!= null ) 
		{
			if(!campo.isEmpty())
			{
				int miaux= campo.indexOf("|");
				tipoPre = campo.substring(miaux+1);
				System.out.println("tipoPre:  "+tipoPre);
				//resul = Integer.parseInt(tipoPre);
				//System.out.println("tipoPre:  "+tipoPre);
			}
		}
			return tipoPre;
		
    }
    /** 
     * cambiaStringF: un string "2797 | 6883615" lo cambiamos "6883615"
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} String campo: un string "2797 | 6883615" lo cambiamos "6883615"
     * 
     * {example} cambiaNumActivo(" "2797 | 6883615"") # 6883615 !.
     * un string "2797 | 6883615" lo cambiamos "6883615"
     * @param campo
     * @return
     */
    public static int cambiaNumActivo(String campo)
    {
    	int resul=0;
    	
    	if (campo!= null ) 
		{
			if(!campo.isEmpty())
			{
				int miaux= campo.indexOf("|");
				String activo = campo.substring(miaux+2);
				System.out.println("activo:  "+activo);
				//campo = numReserva.substring(5);
				resul = Integer.parseInt(activo);
				System.out.println("activo.Inn:  "+resul);
			}
		}
			
			return resul;
		
    }
    
    /** 
     * cambiaPromocion: un string "2797 | 6883615" lo cambiamos "2797"
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} String campo: un string "2797 | 6883615" lo cambiamos "2797"
     * 
     * {example} cambiaPromocion(""2797 | 6883615" # 2797 !.
     * un string "2797 | 6883615" lo cambiamos "2797"
     * @param campo
     * @return
     */
    public static String cambiaPromocion(String campo)
    {
    	String promocion="";
    	
    	if (campo!= null ) 
		{
			if(!campo.isEmpty())
			{
				int miaux= campo.indexOf("|");
				promocion = campo.substring(0,miaux-1);
				System.out.println("promocion:  "+promocion);
			}
		}
			return promocion;
		
    }

/** 
 * MapeaPrecio: un string "DEUDA_NETO" lo cambiamos "23", un string "VALOR_NETO" lo cambiamos por "25" y  un string "VALOR_RAZONABLE" lo cambiamos por "24"
 * 
 * 
 * {talendTypes} String
 * 
 * {Category} User Defined
 * 
 * {param}un string "DEUDA_NETO" lo cambiamos "23", un string "VALOR_NETO" lo cambiamos por "25" y  un string "VALOR_RAZONABLE" lo cambiamos por "24"
 * 
 * {example} MapeaPrecio("DEUDA_NETO" # 23 !.
 * un string "DEUDA_NETO" lo cambiamos "23", un string "VALOR_NETO" lo cambiamos por "25" y  un string "VALOR_RAZONABLE" lo cambiamos por "24"
 * @param campo
 * @return
 */
public static String MapeaPrecio(String campo)
{
	String result="";
	
	if (campo!= null ) 
	{
		if(!campo.isEmpty())
		{
			int miaux= campo.indexOf("_");
			String princio = campo.substring(0,miaux);
			System.out.println("promocion:  "+princio);
			if ("DEUDA".equals(princio))
			{
				result ="23";
				System.out.println("23");
				
			}
			else{
				int aux= campo.indexOf("_");
				String fin = campo.substring(aux+1);
				System.out.println("final:  "+fin);
				if ("NETO".equals(fin))
				{
					result ="25";
					System.out.println("25");
				}
				else {
					result ="24";
					System.out.println("24");
				}
			}
		}
	}
	return result;	
	
}
}