#!/bin/sh
cd `dirname $0`
ROOT_PATH=`pwd`
java -DConfig.dir=/Users/gestycontrol/Documents/Talend/ -DConfig.file.mask=config.ini -Xms256M -Xmx1024M -cp .:$ROOT_PATH:$ROOT_PATH/../lib/routines.jar:$ROOT_PATH/../lib/activation.jar:$ROOT_PATH/../lib/dom4j-1.6.1.jar:$ROOT_PATH/../lib/log4j-1.2.16.jar:$ROOT_PATH/../lib/mail-1.4.jar:$ROOT_PATH/../lib/ojdbc6.jar:$ROOT_PATH/../lib/talend-oracle-timestamptz.jar:$ROOT_PATH/../lib/talend_file_enhanced_20070724.jar:$ROOT_PATH/../lib/talendcsv.jar:$ROOT_PATH/cargagastos_0_1.jar: local_project.cargagastos_0_1.CargaGastos  --context=Default "$@" 