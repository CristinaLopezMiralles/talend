$fileDir = Split-Path -Parent $MyInvocation.MyCommand.Path
cd $fileDir
java '-Xms256M' '-Xmx1024M' '-Dfile.encoding=UTF-8' -cp '.;../lib/routines.jar;../lib/dom4j-1.6.1.jar;../lib/jxl.jar;../lib/log4j-1.2.16.jar;../lib/ojdbc6.jar;../lib/talend-oracle-timestamptz.jar;../lib/talendcsv.jar;comparastock2local_0_1.jar;' local_project.comparastock2local_0_1.comparaStock2local  %* 