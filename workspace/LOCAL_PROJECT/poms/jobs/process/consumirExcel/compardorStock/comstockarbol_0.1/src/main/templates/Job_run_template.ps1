$fileDir = Split-Path -Parent $MyInvocation.MyCommand.Path
cd $fileDir
java '-Xms256M' '-Xmx1024M' -cp '.;../lib/routines.jar;../lib/dom4j-1.6.1.jar;../lib/jxl.jar;../lib/log4j-1.2.16.jar;../lib/ojdbc6.jar;../lib/talend-oracle-timestamptz.jar;../lib/talendcsv.jar;comstockarbol_0_1.jar;' local_project.comstockarbol_0_1.comStockArbol  --context=Default %* 