#!/bin/sh
cd `dirname $0`
ROOT_PATH=`pwd`
java -Xms256M -Xmx1024M -Dfile.encoding=UTF-8 -cp .:$ROOT_PATH:$ROOT_PATH/../lib/routines.jar:$ROOT_PATH/../lib/dom4j-1.6.1.jar:$ROOT_PATH/../lib/log4j-1.2.16.jar:$ROOT_PATH/../lib/ojdbc6.jar:$ROOT_PATH/../lib/ojdbc7.jar:$ROOT_PATH/../lib/talend-oracle-timestamptz.jar:$ROOT_PATH/prinexrem_ventas_0_1.jar: local_project.prinexrem_ventas_0_1.prinexRem_ventas  "$@" 